extends Node2D

signal clicked(pos)

export var text = "Example"
export(String, "Gray", "Pink", "Green", "Orange") var color
export(String, "Text", "Pause") var type
export var offSetX = 0
export var offSetY = 0

var start_position = Vector2()
var spButton
var label
var audio
export(bool) var disabled = false
export(bool) var auto_lock = true

var width = 200
var height = 100

#Buttons
var button_gray = preload("res://Assets/TextGray.png")
var button_gray_clicked = preload("res://Assets/TextGrayClicked.png")
var button_green = preload("res://Assets/TextGreen.png")
var button_green_clicked = preload("res://Assets/TextGreenClicked.png")
var button_pink = preload("res://Assets/TextPink.png")
var button_pink_clicked = preload("res://Assets/TextPinkClicked.png")
var button_orange = preload("res://Assets/TextOrange.png")
var button_orange_clicked = preload("res://Assets/TextOrangeClicked.png")

var button_pause_gray = preload("res://Assets/ButtonPause.png")
var button_pause_clicked_gray = preload("res://Assets/ButtonPauseClicked.png")

var button_normal
var button_clicked



# Called when the node enters the scene tree for the first time.
func _ready():
	spButton = get_node("SpButton")
	label = get_node("Label")
	audio = get_node("AudioClick")
	label.text = text
	$AudioClick.volume_db = Global.sound_db
	
	match color:
		"Green":
			button_normal = button_green
			button_clicked = button_green_clicked
		"Pink":
			button_normal = button_pink
			button_clicked = button_pink_clicked
		"Orange":
			button_normal = button_orange
			button_clicked = button_orange_clicked
		_:
			button_normal = button_gray
			button_clicked = button_gray_clicked
	
	if(type == "Pause"):
		button_normal = button_pause_gray
		button_clicked = button_pause_clicked_gray
		width = 100
		height = 100
		
	#adjusts the values of the limits if the parent node is scaled 
	width *= scale.x
	height *= scale.y
	
	#print(str("width:",width," height:",height))
	#print(position)
	#print(str("button VW ",(1024-get_viewport_rect().size.x)/2))
	#print(str("button VW ",(600-get_viewport_rect().size.y)/2))
	offSetX += (1024-get_viewport_rect().size.x)/2
	offSetY += (600-get_viewport_rect().size.y)/2
	spButton.set_texture(button_normal)
	


func _input(event):
	if not (event is InputEventScreenTouch or event is InputEventMouseButton):
		return
	if event.pressed:
		
		if _checkIfInside(event.position):
			spButton.set_texture(button_clicked)
			start_position = event.position
		else:
			spButton.set_texture(button_normal)
			start_position = null
	else:
		spButton.set_texture(button_normal)
		if start_position != null && _checkIfInside(event.position):
			if not disabled:
				emit_signal("clicked", event.position)
				audio.play(0.1)
				if auto_lock:
					disabled = true
		
func _checkIfInside(pos):
	if((pos.x + offSetX) > position.x && (pos.y + offSetY) > position.y):
		if((pos.x + offSetX) < position.x + width && (pos.y + offSetY) < position.y + height):
			return true
	return false

func setText(text: String):
	label.text = text
	
func setDisabled(disabled: bool):
	self.disabled = disabled
