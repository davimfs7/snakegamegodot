extends Area2D

var fruit1 = preload("res://Assets/red-apple.png")
var fruit2 = preload("res://Assets/pear.png")
var fruit3 = preload("res://Assets/peach.png")
var fruit4 = preload("res://Assets/black-berry-light.png")
var fruit5 = preload("res://Assets/orange.png")

func setNumber(number):
	match number:
		2:
			$Sprite.texture = fruit2
		3:
			$Sprite.texture = fruit3
		4:
			$Sprite.texture = fruit4
		5:
			$Sprite.texture = fruit5
			