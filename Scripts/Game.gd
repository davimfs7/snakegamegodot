extends Node2D

#https://www.free-stock-music.com/arthur-vyncke-seaside-campfire-night.html
#https://godotengine.org/qa/6374/how-to-get-current-platform-os
#String get_name() const
#Return the name of the host OS. Possible values are: 
#"Android", "BlackBerry 10", "Flash", "Haiku", "iOS", "HTML5", "OSX", "Server", "Windows", "WinRT", "X11"

signal swiped(direction)
signal swiped_canceled(start_position)

export(float, 1.0, 1.5) var MAX_DIAGONAL_SLOPE = 1.3

var boardGame = {}
const MIN_X = 138
const MAX_X = 838
const MIN_Y = 180
const MAX_Y = 580
var questions = []
var current_question
var points = 0

onready var timer = $Timer
var swipe_start_position = Vector2()

var lpos
var snake
var pause
var gamePaused = false
var gameStarted = false
var gameFinished = false
var menuPos = "left"
var pressingKey	= false

onready var fruit = preload("res://Scenes/Fruit.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	Firebase.get_document("Questions", $HTTPRequest)
	Global.play_theme2()
	$Points/AudioSwallow.volume_db = Global.sound_db
	$Points/AudioWrong.volume_db = Global.sound_db
	$Start/AudioDead.volume_db = Global.sound_db
	$Start/AudioStart.volume_db = Global.sound_db
	$Start/AudioStart2.volume_db = Global.sound_db
	init_board()
	lpos = get_node("LabelPosition")
	snake = get_node("Snake")
	snake.add_board(self);
	snake.init_body()
	pause = get_node("Pause")
	points = 0
	#print(str("game VW ",get_viewport_rect()))
	#print(str("root size ",get_tree().get_root().size))
	var view_width = get_viewport().get_visible_rect().size.x
	var view_height = get_viewport().get_visible_rect().size.y
	#print(str(view_width, " " , view_height))
	

func _input(event):
	if event is InputEventKey and event.scancode == 80: #P key
		if not pressingKey:
			pressingKey = true
			if gamePaused:
				if not gameStarted:
					return
				unpause()
			else:
				if not gameStarted:
					return
				pause()
		if not event.pressed:
			pressingKey = false
	if event is InputEventKey and event.scancode == 77: #M key
		if not event.pressed:
			snake.add_tail()
	if event is InputEventKey and event.scancode == 78: #N key
		if not event.pressed:
			#print(boardGame)
			print(questions.size())
	if event is InputEventKey and event.scancode == 66: #B key
		if not event.pressed:
			for y in range(0, 9):
				var t = ""
				for x in range(0, 15):
					t+= str(boardGame[Vector2(MIN_X + x * 50, MIN_Y + y * 50)], " ")
				print(t)
	if not event is InputEventScreenTouch:
		return
	if event.pressed:
		_start_detection(event.position)
	elif not timer.is_stopped():
		_end_detection(event.position)

func _start_detection(position):
	swipe_start_position = position
	timer.start()

func _end_detection(position):
	timer.stop()
	var direction = (position - swipe_start_position).normalized()
	if abs(direction.x) + abs(direction.y) >= MAX_DIAGONAL_SLOPE:
		return
	
	#print("---------- Swipe Controls ----------")
	#print(str("Start Point ", swipe_start_position))
	#print(str("Finish Point ", position))
	#print(str("Direction Point ", direction))
	#print("------------------------------------")
		
	if abs(direction.x) > abs(direction.y):
		emit_signal('swiped', Vector2(sign(direction.x), 0.0))
	else:
		emit_signal('swiped', Vector2(0.0, sign(direction.y)))
			
	
func _process(delta):
	#lpos.text = str("Position" , snake.position)
	pass
	
func printl(text):
	lpos.text = text
	
func _on_Timer_timeout():
	emit_signal("swiped_canceled", swipe_start_position)


func add_fruit(number):
	var f = fruit.instance()
	var spaces = get_list_empty_space()
	if spaces.size() < 5:
		win()
		return
	var p = spaces[get_random_number(spaces.size(), 0)]
	boardGame[p] = str("Fruit", number)
	f.position = p
	f.setNumber(number)
	$FruitsZone.add_child(f)
	
func eat(number):
	if(number == current_question.correct):
		snake.add_tail()
		points += 1
		$Points/LabelPoints.text = str(points)
		$Points/AudioSwallow.play()
		#print("correct!")
	else:
		#snake.add_tail()
		$Points/AudioWrong.play()
		#print("Incorrect!")
	clear_fruits()
	makeQuestion()
	
func clear_fruits():
	for i in range(0, $FruitsZone.get_child_count()):
		$FruitsZone.get_child(i).queue_free()
	for y in range(0, 9):
		for x in range(0, 15):
			var verifyPos = Vector2(MIN_X + x * 50, MIN_Y + y * 50)
			if(boardGame[verifyPos] == "Fruit1"):
				boardGame[verifyPos] = "Empty"
				continue
			if(boardGame[verifyPos] == "Fruit2"):
				boardGame[verifyPos] = "Empty"
				continue
			if(boardGame[verifyPos] == "Fruit3"):
				boardGame[verifyPos] = "Empty"
				continue
			if(boardGame[verifyPos] == "Fruit4"):
				boardGame[verifyPos] = "Empty"
				continue
			if(boardGame[verifyPos] == "Fruit5"):
				boardGame[verifyPos] = "Empty"
				continue
	
	
func get_list_empty_space():
	var spaces = []
	for y in range(0, 9):
		for x in range(0, 15):
			var verifyPos = Vector2(MIN_X + x * 50, MIN_Y + y * 50)
			if(boardGame[verifyPos] == "Empty"):
				if snake.position + Vector2(0,50) == verifyPos:
					continue
				if snake.position + Vector2(0,-50) == verifyPos:
					continue
				if snake.position + Vector2(50,0) == verifyPos:
					continue
				if snake.position + Vector2(-50,0) == verifyPos:
					continue
				spaces.push_back (verifyPos)
	return spaces

func get_random_number(MAX, MIN):
	randomize()
	var num = randi()%MAX + MIN
	return num
	
func init_board():
	for x in range(0, 15):
		for y in range(0, 9):
			boardGame[Vector2(MIN_X + x * 50, MIN_Y + y * 50)] = "Empty"



func _on_ButtonPause_clicked(pos):
	if not gameStarted:
		return
	pause()

func _on_Button_clicked(pos):
	if gameFinished:
		get_tree().reload_current_scene()
	else:
		unpause()
	
func pause():
	$Pause/Button.setDisabled(false)
	$Pause/ButtonQuit.setDisabled(false)
	if snake.position.x < 530:
		if menuPos == "left":
			$Pause/Wooden.position += Vector2(400, 0)
			$Pause/Button.position += Vector2(400, 0)
			$Pause/ButtonQuit.position += Vector2(400, 0)
			$Pause/SpritePause.position += Vector2(400, 0)
			menuPos = "right"
	else:
		if menuPos == "right":
			$Pause/Wooden.position += Vector2(-400, 0)
			$Pause/Button.position += Vector2(-400, 0)
			$Pause/ButtonQuit.position += Vector2(-400, 0)
			$Pause/SpritePause.position += Vector2(-400, 0)
			menuPos = "left"
	pause.visible = true
	gamePaused = true
	
func unpause():
	$ButtonPause.setDisabled(false)
	$Pause/Button.setDisabled(true)
	$Pause/ButtonQuit.setDisabled(true)
	pause.visible = false
	gamePaused = false
	


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	#print(result_body.documents)
	match response_code:
		404:
			#notification.text = "Please, enter your information"
			#$Dialog.popUpInfo(result)
			print(result)
			return
		200:
			
			for i in range(0,result_body.documents.size()):
				var doc = result_body.documents[i].fields
				var frequency = int(doc.Frequency.integerValue)
				var q = {}
				q.question = doc.Question.stringValue
				q.duration = int(doc.Duration.integerValue)
				q.answer1 = doc.Answer1.stringValue
				q.answer2 = doc.Answer2.stringValue
				q.answer3 = doc.Answer3.stringValue
				q.answer4 = doc.Answer4.stringValue
				q.answer5 = doc.Answer5.stringValue
				q.correct = int(doc.Correct.integerValue)
				for i in range(frequency):
					questions.push_back(q)
					


func win():
	gameStarted = false
	#todo: win pop-up

func lose():
	$Pause/Button.setText("Play Again")
	$Pause/Button.setDisabled(false)
	$Pause/ButtonQuit.setDisabled(false)
	gameStarted = false
	$AnimationPlayer.playback_speed = 1
	$AnimationPlayer.play("Lose")
	gameFinished = true

func _on_ButtonQuit_clicked(pos):
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Curtain":
		$AnimationPlayer.playback_speed = 1
		$AnimationPlayer.play("Start")
	if anim_name == "Start":
		$Start.visible = false
		gameStarted = true
		makeQuestion()
	if anim_name == "Cooldown":
		gameStarted = true

func beep_start():
	$Start/AudioStart.play()
	
func beep_start2():
	$Start/AudioStart2.play()
	
func beep_dead():
	$Start/AudioDead.play()
	
func makeQuestion():
	current_question = questions[get_random_number(questions.size(),0)]
	$Question/LabelQuestion.text = current_question.question
	gameStarted = false
	
	if current_question.answer1.strip_edges() != "":
		$List/LabelAnswer1.text = current_question.answer1.strip_edges()
		$List/LabelAnswer1.visible = true
		$List/SPAnswer1.visible = true
		add_fruit(1)
	else:
		$List/LabelAnswer1.visible = false
		$List/SPAnswer1.visible = false
		
	
	if current_question.answer2.strip_edges() != "":
		$List/LabelAnswer2.text = current_question.answer2.strip_edges()
		$List/LabelAnswer2.visible = true
		$List/SPAnswer2.visible = true
		add_fruit(2)
	else:
		$List/LabelAnswer2.visible = false
		$List/SPAnswer2.visible = false
		
	if current_question.answer3.strip_edges() != "":
		$List/LabelAnswer3.text = current_question.answer3.strip_edges()
		$List/LabelAnswer3.visible = true
		$List/SPAnswer3.visible = true
		add_fruit(3)
	else:
		$List/LabelAnswer3.visible = false
		$List/SPAnswer3.visible = false
		
	if current_question.answer4.strip_edges() != "":
		$List/LabelAnswer4.text = current_question.answer4.strip_edges()
		$List/LabelAnswer4.visible = true
		$List/SPAnswer4.visible = true
		add_fruit(4)
	else:
		$List/LabelAnswer4.visible = false
		$List/SPAnswer4.visible = false
		
	if current_question.answer5.strip_edges() != "":
		$List/LabelAnswer5.text = current_question.answer5.strip_edges()
		$List/LabelAnswer5.visible = true
		$List/SPAnswer5.visible = true
		add_fruit(5)
	else:
		$List/LabelAnswer5.visible = false
		$List/SPAnswer5.visible = false
		
	$AnimationPlayer.playback_speed = 5000.0 / current_question.duration
	$AnimationPlayer.play("Cooldown")
