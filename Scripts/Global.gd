extends Node

var main_theme1 = preload("res://Sound/main_theme.wav")
var main_theme2 = preload("res://Sound/main_theme2.wav")
var db = -20.0
var sound_db = -20.0
var music
var gameStarted = true

func _ready():
	music = AudioStreamPlayer.new()
	#music.stop()
	add_child(music)
	#music.stream = main_theme1
	music.volume_db = db
	music.connect("finished", self, "finished")
	#music.play()
	pass
	
func set_db(db):
	self.db = db
	music.volume_db = db

func set_sound_db(db):
	sound_db = db

func play_theme1():
	if gameStarted == false:
		return
	music.stop()
	music.stream = main_theme1
	music.volume_db = db
	music.play()
	gameStarted = false
	
func play_theme2():
	music.stop()
	music.stream = main_theme2
	music.volume_db = db
	music.play()
	gameStarted = true

func finished():
	music.play()
