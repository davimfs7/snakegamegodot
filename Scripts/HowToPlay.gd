extends Node2D


func _ready():
	if OS.get_name() == "Android" or OS.get_name() == "iOS" or OS.get_name() == "BlackBerry 10":
		$Instructions/Swipe.visible = true
		$Instructions/Arrows.visible = false
		$Instructions/LabelMove.text = "Swipe to move"
		$Instructions/LabelPause.text = "Button pause \nto pause"

func _on_Button_clicked(pos):
	get_tree().change_scene("res://Scenes/Menu.tscn")
