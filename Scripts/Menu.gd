extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	if not Firebase.log_in:
		Firebase.login("system@mail.com", "D%7]Y(MtG$>P", $HTTPRequest)
	Global.play_theme1()
	if OS.get_name() == "HTML5":
		$Quit.visible = false
		
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	print("DEU!")


func _on_Button_clicked(pos):
	if Firebase.log_in:
		var curtain = get_node("AnimationCurtain")
		curtain.play("Curtain")


func _on_AnimationCurtain_animation_finished(anim_name):
	#https://godotengine.org/qa/24773/how-to-load-and-change-scenes
	get_tree().change_scene("res://Scenes/Game.tscn")


func _on_Quit_clicked(pos):
	get_tree().quit()


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var response_body = JSON.parse(body.get_string_from_ascii())
	if response_code != 200:
		print(response_body.result.error.message.capitalize())
	else:
		$Play.setDisabled(false)
		#ready = true
		#print("Login successful")


func _on_HowToPlay_clicked(pos):
	get_tree().change_scene("res://Scenes/HowToPlay.tscn")


func _on_Settings_clicked(pos):
	get_tree().change_scene("res://Scenes/Settings.tscn")
