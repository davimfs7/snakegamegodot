extends Node2D

var sp_volume = preload("res://Assets/volume.png")
var sp_volume_off = preload("res://Assets/volume_off.png")
var sp_music = preload("res://Assets/music.png")
var sp_music_off = preload("res://Assets/music_off.png")

func _on_Slider_valueUpdated(val):
	var new_db = (val * 104) / 100
	new_db -= 80
	Global.set_db(new_db)
	
	if new_db == -80:
		$SpriteVolume.texture = sp_volume_off
	else:
		$SpriteVolume.texture = sp_volume
		

func _on_Slider2_valueUpdated(val):
	var new_db = (val * 104) / 100
	new_db -= 80
	Global.set_sound_db(new_db)
	$Button/AudioClick.volume_db = new_db
	$Button2/AudioClick.volume_db = new_db
	
	if new_db == -80:
		$SpriteMusic.texture = sp_music_off
	else:
		$SpriteMusic.texture = sp_music

func _on_Button_clicked(pos):
	get_tree().change_scene("res://Scenes/Menu.tscn")


