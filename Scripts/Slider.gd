extends Node2D

signal valueUpdated(val)

const offSetXPoint = 35

export(String, "Purple", "Green") var color
export (float) var minimum = 0.0
export (float) var maximum = 100.0
export (float) var value = 50.0
export var offSetX = 0
export var offSetY = 0

var width = 260
var height = 75

var clicked = false

var green_point = preload("res://Assets/round_green.png")

func _ready():
	if color == "Green":
		$Point.texture = green_point

func _input(event):
	if event is InputEventScreenTouch or event is InputEventMouseButton:
		if _checkIfInside(event.position):
			clicked = event.is_pressed()
			updateBar(event.position)
		else:
			clicked = false
		if not event.is_pressed():
			emit_signal("valueUpdated", value)
	if event is InputEventScreenDrag or event is InputEventMouseMotion:
		if clicked:
			updateBar(event.position)

func updateBar(pos):
	var relativeX = (pos.x + offSetX) - position.x
	if relativeX >= 0 and relativeX <= width:
		var val = relativeX - offSetXPoint
		$Point.position = Vector2(val, $Point.position.y)
		if val <= 0:
			val = -0 
		if val >= 200:
			val = 200
		value = (val * 100) / 200
		#print(val) 

func _checkIfInside(pos):
	if((pos.x + offSetX) > position.x && (pos.y + offSetY) > position.y):
		if((pos.x + offSetX) < position.x + width && (pos.y + offSetY) < position.y + height):
			return true
	return false
