extends Node2D

var jump = 50
var speed = 0.500
var prev_dir
var cur_dir = Vector2(jump,0)
var angle = -PI/2

var snake_dead = preload("res://Assets/SnakeDead.png")
onready var tail = preload("res://Scenes/Tail.tscn")
var child_tail
var board

var time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	rotation = -PI/2
	pass

func add_board(b):
	board = b

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(board.gamePaused or not board.gameStarted):
		return
	var tem_dir = cur_dir
	var temp_angle = angle
	if(Input.is_action_just_pressed("ui_up")):
		#move up
		cur_dir = Vector2(0,-jump);
		angle = PI
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	elif(Input.is_action_just_pressed("ui_down")):
		#move down
		cur_dir = Vector2(0,jump);
		angle = 0
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	elif(Input.is_action_just_pressed("ui_left")):
		#move left
		cur_dir = Vector2(-jump,0);
		angle = PI/2
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	elif(Input.is_action_just_pressed("ui_right")):
		#move right
		cur_dir = Vector2(jump,0);
		angle = -PI/2
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
		
	
	
		
	time += delta
	#print("elapsed : " , time)
	
	if(time > speed):
		time -= speed
		move()
	pass
	
func move():
	gonna_eat()
	if not gonna_die():
		board.boardGame[position] = "Empty"
		position += cur_dir
		board.boardGame[position] = "Snake"
		prev_dir = cur_dir
		rotation = angle
		if(child_tail != null):
			child_tail.move(cur_dir, angle)
	else:
		$head/Sprite.texture = snake_dead
		board.lose()

func gonna_die():
	var nextPos = position+cur_dir
	for key in board.boardGame:
		if key == nextPos:
			if board.boardGame[nextPos] == "Empty":
				return false
			elif "Fruit" in board.boardGame[nextPos]:
				return false
			else:
				return true
	if nextPos.x < 138 or nextPos.x > 838:
		return true
	if nextPos.y < 180 or nextPos.y > 580:
		return true
	return false

func gonna_eat():
	var nextPos = position+cur_dir
	for key in board.boardGame:
		if key == nextPos:
			if board.boardGame[nextPos] == "Fruit1":
				board.eat(1)
				increase_speed()
				return
			if board.boardGame[nextPos] == "Fruit2":
				board.eat(2)
				increase_speed()
				return
			if board.boardGame[nextPos] == "Fruit3":
				board.eat(3)
				increase_speed()
				return
			if board.boardGame[nextPos] == "Fruit4":
				board.eat(4)
				increase_speed()
				return
			if board.boardGame[nextPos] == "Fruit5":
				board.eat(5)
				increase_speed()
				return
	
func increase_speed():
	if ( 0.500 >= speed && speed > 0.400 ):
		speed -= 0.045
	if ( 0.400 >= speed && speed > 0.350 ):
		speed -= 0.022
	if ( 0.350 >= speed && speed > 0.300 ):
		speed -= 0.015
	if ( 0.300 >= speed && speed > 0.250 ):
		speed -= 0.01
	if ( 0.250 >= speed && speed > 0.200 ):
		speed -= 0.005
	if ( 0.200 >= speed && speed > 0.150 ):
		speed -= 0.003
	if ( 0.150 >= speed && speed > 0.125 ):
		speed -= 0.002
	if ( 0.125 >= speed && speed > 0.110 ):
		speed -= 0.001
	if ( 0.110 >= speed && speed > 0.99 ):
		speed -= 0.0005
	#print(speed)

func add_tail():
	if(child_tail == null):
		child_tail = tail.instance()
		child_tail.position = position
		child_tail.rotation = angle
		child_tail.board = board
		board.add_child(child_tail)
	else:
		child_tail.add_tail()
	
func init_body():
	add_tail()
	move()
	add_tail()
	move()

func _on_Button_pressed():
	#rotation += PI/2
	add_tail()

func _on_Game_swiped(direction):
	
	if(board.gamePaused or not board.gameStarted):
		return
	
	var tem_dir = cur_dir
	var temp_angle = angle
	
	if(direction == Vector2(1,0)):
		cur_dir = Vector2(jump,0);
		angle = -PI/2
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	if(direction == Vector2(-1,0)):
		cur_dir = Vector2(-jump,0);
		angle = PI/2
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	if(direction == Vector2(0,1)):
		cur_dir = Vector2(0,jump);
		angle = 0
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
	if(direction == Vector2(0,-1)):
		cur_dir = Vector2(0,-jump);
		angle = PI
		if gonna_die():
			cur_dir = tem_dir
			angle = temp_angle
		else:
			move()
			time = 0
