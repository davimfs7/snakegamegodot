extends Area2D

var board
var tail
var child_tail

#textures
var sprite
var snake_body = preload("res://Assets/SnakeBody.png")
var snake_body_turn = preload("res://Assets/SnakeBodyTurn.png")
var snake_body_turn_inside = preload("res://Assets/SnakeBodyTurnInside.png")
var snake_body_tail = preload("res://Assets/SnakeBodyTail.png")

var prev_dir
var cur_dir
var prev_angle
var cur_angle

func _init():
	tail = load("res://Scenes/Tail.tscn")
	visible = false
	
func _ready():
	sprite = get_node("Sprite")

func add_tail():
	if(child_tail == null):
		child_tail = tail.instance()
		child_tail.position = position
		child_tail.board = board
		if(cur_angle == null):
			cur_angle = 0
		child_tail.rotation = cur_angle
		board.add_child(child_tail)
	else:
		child_tail.add_tail()


func move(dir, angle):
	
	var bAngle = 0
	
	if(child_tail != null):
		if cur_angle != null && angle != null:
			if(cur_angle != angle):
				#turn RIGHT from BOTTOM (right)
				if(angle == -PI/2 && cur_angle == PI):
					sprite.set_texture(snake_body_turn)
					#print("R4!")
				#turn UP from LEFT (left)
				elif(angle == PI && cur_angle == -PI/2):
					sprite.set_texture(snake_body_turn_inside)
					#bAngle = -PI/2
					#print("L1!")
				# turn left
				elif (cur_angle > angle):
					sprite.set_texture(snake_body_turn_inside)
					#bAngle = -PI/2
					#print("L")
				#turn right
				else:
					sprite.set_texture(snake_body_turn)
					#print("R")
			else:
				sprite.set_texture(snake_body)
				pass
	else:
		sprite.set_texture(snake_body_tail)
		
	if (cur_dir != null):
		board.boardGame[position] = "Empty"
		position += cur_dir
		board.boardGame[position] = "Tail"
		rotation = angle + bAngle
		
	prev_dir = cur_dir
	prev_angle = cur_angle
	cur_dir = dir
	cur_angle = angle
	
	if(child_tail != null):
		child_tail.move(prev_dir, prev_angle)
		
	if not visible:
		visible = true
